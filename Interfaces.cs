﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TETRIS
{
    public interface IDrawable
    {
        Texture2D Texture { get; }
        Vector2 Bounds { get; }
    }

    public interface IScreen
    {
        bool UpdateActive { get; set; }
        bool DrawActive { get; set; }

        void Update(GameTime gameTime);
        void Draw(GameTime gameTime);
    }

    public interface IUpdateable
    {
        void Update();
    }
}
