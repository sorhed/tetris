﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace TETRIS
{
    //enums

    #region enums

    public enum CollisionKind
    {
        Tetromino = 0,
        RightBound = 1,
        LeftBound = 2,
        Bottom = 3,
        UpperBound = 4
    } // kinds of collision the game knows how to resolve

    public enum MovementKind
    {
        Right = 1,
        Left = 2,
        Up = 3,
        Down = 4,
        Rotate = 5,
        Teleport = 6
    } // kinds of non-prohibited transitions GameObjects can perform

    public enum ShapeKind
    {
        O = 2,
        L = 0,
        J = 1,
        I = 3,
        S = 4,
        Z = 5,
        T = 6
    } // kinds of shape the game knows how to work with

    public enum SmartShapeKind
    {
        Falling,
        Preview,
        Ghost
    } //uses that shape could have

    public enum BrickState
    {
        Falling,
        Laying,
        Dead,
        Ghost
    } // kinds of state the brick can be in

    public enum Message
    {
        Moved,
        Landed,
        MarkedDead,
        HitUpperBound
    } //kinds of messages the shape can send

    public enum GameState
    {
        GameOver,
        TheGame,
        PauseMenu,
        MainMenu,
        ScoreTable
    }

    #endregion

    //GameObjects blueprints [data containers only]

    #region GameObjects

    public class Brick : IDrawable
    {
        public Rectangle bounds;
        public BrickState state;
        private readonly Texture2D fallingTexture, layingTexture, deadTexture;
        private Texture2D texture;
        public Texture2D Texture => texture;
        public Vector2 Bounds => new Vector2(bounds.X, bounds.Y);

        public Brick(Texture2D texture, Rectangle bounds)
        {
            this.bounds = bounds;
            this.texture = texture;
        }

        public Brick(Texture2D fallingTexture, Texture2D layingTexture, Texture2D deadTexture, Rectangle bounds)
        {
            this.bounds = bounds;
            this.fallingTexture = fallingTexture;
            this.layingTexture = layingTexture;
            this.deadTexture = deadTexture;
        }

        public void SetTexture(Texture2D texture)
        {
            this.texture = texture;
        }

        public void UpdateTextures()
        {
            switch (state)
            {
                    case BrickState.Falling:
                    texture = fallingTexture;
                    break;
                    case BrickState.Laying:
                    texture = layingTexture;
                    break;
                    case BrickState.Dead:
                    texture = deadTexture;
                    break;
            }
        }
    }

    public class Shape
    {
        private Point centre;
        public ShapeKind ShapeKind { get; private set; }
        public List<Brick> Bricks;
        public Point Centre => centre;
        public List<Rectangle> OldBounds { get; set; }


        public Shape(ShapeKind shapeKind, List<Brick> bricks, Point centre)
        {
            if (shapeKind == ShapeKind.O)
                throw new ArgumentException("For the O shape use the constructor with two params");
            ShapeKind = shapeKind;
            Bricks = bricks;
            this.centre = centre;
        }

        public Shape(ShapeKind shapeKind, List<Brick> bricks)
        {
            if (shapeKind != ShapeKind.O)
                throw new ArgumentException("For shapes other than O use the three params contructor");
            Bricks = bricks;
            ShapeKind = shapeKind;
        }

        public void ModifyCentre(int x, int y)
        {
            centre.X += x;
            centre.Y += y;
        }

    }


    #endregion

    //spawning logic

    #region spawners

    public abstract class ShapeMaker
    {
        protected int brickSize;
        protected Rectangle spawningPoint;
        protected Texture2D texture;
        protected List<Brick> tempBricks = new List<Brick>();
        protected Point Centre;

        protected ShapeMaker(Rectangle spawningPoint, Texture2D texture)
        {
            brickSize = new BoardVariableHolder().BrickSize;
            this.spawningPoint = spawningPoint;
            this.texture = texture;
        }

        public abstract Shape Execute();
    }

    public class MakeShapeZ : ShapeMaker
    {
        public MakeShapeZ(Rectangle spawningPoint, Texture2D texture)
            : base(spawningPoint, texture)
        {
        }

        public override Shape Execute()
        {
            for (int x = 0; x < 2; x++)
            {
                Rectangle rectangle = new Rectangle(spawningPoint.X + x*(brickSize), spawningPoint.Y, brickSize,
                    brickSize);
                tempBricks.Add(new Brick(texture, rectangle));
                if (x == 1)
                {
                    Centre.X = rectangle.X + brickSize/2;
                    Centre.Y = rectangle.Y + brickSize/2;
                }
            }
            for (int x = 2; x < 4; x++)
            {
                Rectangle rectangle = new Rectangle(spawningPoint.X + (x - 1)*(brickSize), spawningPoint.Y + brickSize,
                    brickSize, brickSize);
                tempBricks.Add(new Brick(texture, rectangle));
            }

            return new Shape(ShapeKind.Z, tempBricks, Centre);
        }
    }

    public class MakeShapeI : ShapeMaker
    {
        public MakeShapeI(Rectangle spawningPoint, Texture2D texture)
            : base(spawningPoint, texture)
        {
        }

        public override Shape Execute()
        {
            for (int x = 0; x < 4; x++)
            {
                Rectangle rectangle = new Rectangle(spawningPoint.X + x*brickSize, spawningPoint.Y, brickSize, brickSize);
                tempBricks.Add(new Brick(texture, rectangle));
                if (x == 2)
                {
                    Centre.X = rectangle.X + brickSize/2;
                    Centre.Y = rectangle.Y + brickSize/2;
                }
            }

            return new Shape(ShapeKind.I, tempBricks, Centre);
        }
    }

    public class MakeShapeL : ShapeMaker
    {
        public MakeShapeL(Rectangle spawningPoint, Texture2D texture)
            : base(spawningPoint, texture)
        {
        }

        public override Shape Execute()
        {
            for (int x = 0; x < 3; x++)
            {
                Rectangle rectangle = new Rectangle(spawningPoint.X + x*(brickSize), spawningPoint.Y, brickSize,
                    brickSize);
                tempBricks.Add(new Brick(texture, rectangle));
                if (x == 1)
                {
                    Centre.X = rectangle.X + brickSize/2;
                    Centre.Y = rectangle.Y + brickSize/2;
                }
                if (x == 2)
                {
                    Rectangle _rectangle = new Rectangle(spawningPoint.X + x*(brickSize),
                        spawningPoint.Y - (brickSize), brickSize, brickSize);
                    tempBricks.Add(new Brick(texture, _rectangle));
                }
            }
            return new Shape(ShapeKind.L, tempBricks, Centre);
        }
    }

    public class MakeShapeJ : ShapeMaker
    {
        public MakeShapeJ(Rectangle spawningPoint, Texture2D texture)
            : base(spawningPoint, texture)
        {
        }

        public override Shape Execute()
        {
            for (int x = 0; x < 3; x++)
            {
                Rectangle rectangle = new Rectangle(spawningPoint.X + x*(brickSize), spawningPoint.Y, brickSize,
                    brickSize);
                tempBricks.Add(new Brick(texture, rectangle));
                if (x == 1)
                {
                    Centre.X = rectangle.X + brickSize/2;
                    Centre.Y = rectangle.Y + brickSize/2;
                }
                if (x == 0)
                {
                    Rectangle _rectangle = new Rectangle(spawningPoint.X + x*(brickSize), spawningPoint.Y - (brickSize),
                        brickSize, brickSize);
                    tempBricks.Add(new Brick(texture, _rectangle));
                }

            }
            return new Shape(ShapeKind.J, tempBricks, Centre);
        }
    }

    public class MakeShapeS : ShapeMaker
    {
        public MakeShapeS(Rectangle spawningPoint, Texture2D texture)
            : base(spawningPoint, texture)
        {
        }

        public override Shape Execute()
        {
            for (int x = 0; x < 2; x++)
            {
                Rectangle rectangle = new Rectangle(spawningPoint.X + x*(brickSize), spawningPoint.Y, brickSize,
                    brickSize);
                tempBricks.Add(new Brick(texture, rectangle));
                if (x == 0)
                {
                    Centre.X = rectangle.X + brickSize/2;
                    Centre.Y = rectangle.Y + brickSize/2;
                }
            }
            for (int x = 0; x < 2; x++)
            {
                Rectangle rectangle = new Rectangle(spawningPoint.X - x*(brickSize), spawningPoint.Y + brickSize,
                    brickSize, brickSize);
                tempBricks.Add(new Brick(texture, rectangle));
            }
            return new Shape(ShapeKind.S, tempBricks, Centre);
        }
    }

    public class MakeShapeO : ShapeMaker
    {
        public MakeShapeO(Rectangle spawningPoint, Texture2D texture)
            : base(spawningPoint, texture)
        {
        }

        public override Shape Execute()
        {
            for (int y = 0; y < 2; y++)
            {
                Rectangle rectangle = new Rectangle(spawningPoint.X, spawningPoint.Y + y*(brickSize), brickSize,
                    brickSize);
                tempBricks.Add(new Brick(texture, rectangle));
            }
            for (int x = 0; x < 2; x++)
            {
                Rectangle rectangle = new Rectangle(spawningPoint.X + (brickSize), spawningPoint.Y + x*(brickSize),
                    brickSize, brickSize);
                tempBricks.Add(new Brick(texture, rectangle));
            }

            return new Shape(ShapeKind.O, tempBricks);
        }
    }

    public class MakeShapeT : ShapeMaker
    {
        public MakeShapeT(Rectangle spawningPoint, Texture2D texture)
            : base(spawningPoint, texture)
        {
        }

        public override Shape Execute()
        {
            for (int x = 0; x < 3; x++)
            {
                Rectangle rectangle = new Rectangle(spawningPoint.X + x*(brickSize), spawningPoint.Y, brickSize,
                    brickSize);
                tempBricks.Add(new Brick(texture, rectangle));
                if (x == 1)
                {
                    Centre.X = rectangle.X + brickSize/2;
                    Centre.Y = rectangle.Y + brickSize/2;
                }
                if (x == 1)
                {
                    Rectangle _rectangle = new Rectangle(spawningPoint.X + x*(brickSize),
                        spawningPoint.Y - (brickSize), brickSize, brickSize);
                    tempBricks.Add(new Brick(texture, _rectangle));
                }

            }

            return new Shape(ShapeKind.T, tempBricks, Centre);
        }
    }

    // the output class (in order to spawn non-custom shapes use variable.SetKind(yourKind).AutoSpawn(shapeKind), or assign texture and spawning point manually: variable.ManualSpawn())

    public class ShapeSpawner
    {
        private Texture2D texture;
        private ShapeMaker shapeMaker;
        private Rectangle spawningPoint;

        public ShapeSpawner SetKind(SmartShapeKind smartShapeKind)
        {
            var variablePasser = new BoardVariableHolder();

            switch (smartShapeKind)
            {
                case SmartShapeKind.Falling:
                    texture = Game1.RegularBrickTexture;
                    spawningPoint = variablePasser.StartingSpawningPoint;
                    return this;
                case SmartShapeKind.Ghost:
                    texture = Game1.GhostTexture;
                    spawningPoint = variablePasser.GhostSpawningPoint;
                    return this;
                case SmartShapeKind.Preview:
                    texture = Game1.PreviewTexture;
                    spawningPoint = variablePasser.PreviewShapeSpawningPoint;
                    return this;
                default:
                    throw new ArgumentOutOfRangeException(nameof(smartShapeKind), smartShapeKind, null);
            }
        }

        public Shape ManualSpawn(ShapeKind shapeKind, Texture2D texture, Rectangle spawningPoint)
        {
            switch (shapeKind)
            {
                case ShapeKind.I:
                    shapeMaker = new MakeShapeI(spawningPoint, texture);
                    return shapeMaker.Execute();
                case ShapeKind.J:
                    shapeMaker = new MakeShapeJ(spawningPoint, texture);
                    return shapeMaker.Execute();
                case ShapeKind.L:
                    shapeMaker = new MakeShapeL(spawningPoint, texture);
                    return shapeMaker.Execute();
                case ShapeKind.O:
                    shapeMaker = new MakeShapeO(spawningPoint, texture);
                    return shapeMaker.Execute();
                case ShapeKind.S:
                    shapeMaker = new MakeShapeS(spawningPoint, texture);
                    return shapeMaker.Execute();
                case ShapeKind.Z:
                    shapeMaker = new MakeShapeZ(spawningPoint, texture);
                    return shapeMaker.Execute();
                case ShapeKind.T:
                    shapeMaker = new MakeShapeT(spawningPoint, texture);
                    return shapeMaker.Execute();
                default:
                    throw new ArgumentOutOfRangeException(nameof(shapeKind), shapeKind, null);
            }
        }

        public Shape AutoSpawn(ShapeKind shapeKind)
        {
            if (texture == null) throw new ArgumentNullException();
            switch (shapeKind)
            {
                case ShapeKind.I:
                    shapeMaker = new MakeShapeI(spawningPoint, texture);
                    return shapeMaker.Execute();
                case ShapeKind.J:
                    shapeMaker = new MakeShapeJ(spawningPoint, texture);
                    return shapeMaker.Execute();
                case ShapeKind.L:
                    shapeMaker = new MakeShapeL(spawningPoint, texture);
                    return shapeMaker.Execute();
                case ShapeKind.O:
                    shapeMaker = new MakeShapeO(spawningPoint, texture);
                    return shapeMaker.Execute();
                case ShapeKind.S:
                    shapeMaker = new MakeShapeS(spawningPoint, texture);
                    return shapeMaker.Execute();
                case ShapeKind.Z:
                    shapeMaker = new MakeShapeZ(spawningPoint, texture);
                    return shapeMaker.Execute();
                case ShapeKind.T:
                    shapeMaker = new MakeShapeT(spawningPoint, texture);
                    return shapeMaker.Execute();
                default:
                    throw new ArgumentOutOfRangeException(nameof(shapeKind), shapeKind, null);
            }
        }
    }

    #endregion

    //movement logic

    #region Movement

    //internal low - level movement handler [moves abstract shape / brick with no regard to the gamelogic laws / in order to resolve an in-game transition uaw the TransitionHandler class]

    public class MovementHandler
    {
        public int Move { get; private set; }

        public MovementHandler(int move)
        {
            Move = move > 0 ? move : -move;
        }

        public MovementHandler Reverse()
        {
            Move *= -1;
            return this;
        }

        public void TransitionX(Shape shape)
        {
            shape.Bricks.ForEach(brick => brick.bounds.X += Move);
            shape.ModifyCentre(Move, 0);
            if (Move < 0)
                Move *= -1;
        }

        public void TransitionX(Brick brick)
        {
            brick.bounds.X += Move;
            if (Move < 0)
                Move *= -1;
        }

        public void TransitionY(Shape shape)
        {
            shape.Bricks.ForEach(brick => brick.bounds.Y += Move);
            shape.ModifyCentre(0, Move);
            if (Move < 0)
                Move *= -1;
        }

        public void TransitionY(Brick brick)
        {
            brick.bounds.Y += Move;
            if (Move < 0)
                Move *= -1;
        }

        public void Rotation(Shape shape, Point centre)
        {
            if (Move <= 0) throw new InvalidDataException("Should be unreachable; move < 0");
            foreach (var brick in shape.Bricks)
            {
                var oldPostion = brick.bounds;
                brick.bounds.X = (oldPostion.Y + centre.X - centre.Y);
                brick.bounds.Y = (centre.X + centre.Y - oldPostion.X - brick.bounds.Height);
            }
        }

        public void CloneBrickPosition(Brick brick, Rectangle bounds)
        {
            brick.bounds = bounds;
        }
    }

    //gamelogic transition resolvers [they move given Shape according to the gamelogic laws]

    //input blueprint (it takes Shape)

    public abstract class TransitionManager
    {
        protected Shape ControlledShape;
        protected MovementHandler movementHandler;
        protected CollisionHandler collisionHandler;
        protected Subject listener;
        protected Action<Shape> UpdateOldBounds =
            shape => shape.OldBounds = shape.Bricks.Select(brick => brick.bounds).ToList();  

        
        protected TransitionManager(Shape controlledShape)
        {
            ControlledShape = controlledShape;
            collisionHandler = new CollisionHandler(controlledShape);
            var variablePasser = new BoardVariableHolder();
            movementHandler = new MovementHandler(variablePasser.BrickSize);
            listener = new Subject();
            listener.AddObserver(new Logic());
            listener.AddObserver(new Audio());
        }

        public abstract void Execute();

        public void Undo()
        {
            for (int i = 0; i < 4; i++)
            {
                movementHandler.CloneBrickPosition(ControlledShape.Bricks.ElementAt(i), ControlledShape.OldBounds.ElementAt(i));
            }
        }
    }

    //actual resolvers

    public class TransitionLeft : TransitionManager
    {
        public TransitionLeft(Shape controlledShape) : base(controlledShape)
        {
        }

        public override void Execute()
        {
            UpdateOldBounds(ControlledShape);
            movementHandler.Reverse().TransitionX(ControlledShape);
           if (collisionHandler.Execute(CollisionKind.LeftBound) || collisionHandler.Execute(CollisionKind.Tetromino))
                movementHandler.TransitionX(ControlledShape);
        }
    }

    public class TransitionRight : TransitionManager
    {
        public TransitionRight(Shape controlledShape) : base(controlledShape)
        {
        }

        public override void Execute()
        {
            UpdateOldBounds(ControlledShape);
            movementHandler.TransitionX(ControlledShape);
            if (collisionHandler.Execute(CollisionKind.RightBound) || collisionHandler.Execute(CollisionKind.Tetromino))
               movementHandler.Reverse().TransitionX(ControlledShape);
        }
    }

    public class Rotate : TransitionManager
    {
        public Rotate(Shape controlledShape) : base(controlledShape)
        {
        }

        public override void Execute()
        {
            UpdateOldBounds(ControlledShape);
            if (ControlledShape.ShapeKind == ShapeKind.O) return;

            movementHandler.Rotation(ControlledShape, ControlledShape.Centre);

            while (collisionHandler.Execute(CollisionKind.LeftBound))
            {
                movementHandler.TransitionX(ControlledShape);
                if (collisionHandler.Execute(CollisionKind.LeftBound) == false)
                    break;
            }
            while (collisionHandler.Execute(CollisionKind.RightBound))
            {
                movementHandler.Reverse().TransitionX(ControlledShape);
                if (collisionHandler.Execute(CollisionKind.RightBound) == false)
                    break;
            }

            if ( collisionHandler.Execute(CollisionKind.Tetromino))
                    Undo();
        }
    }

    public class TransitionUp : TransitionManager
    {
        public TransitionUp(Shape controlledShape) : base(controlledShape)
        {
        }

        public override void Execute()
        {
            UpdateOldBounds(ControlledShape);
            movementHandler.Reverse().TransitionY(ControlledShape);
        }
    }

    public class TransitionDown : TransitionManager
    {
        public TransitionDown(Shape controlledShape) : base(controlledShape)
        {
        }

        public override void Execute()
        {
            UpdateOldBounds(ControlledShape);
            movementHandler.TransitionY(ControlledShape);
            if (collisionHandler.Execute(CollisionKind.Bottom) || collisionHandler.Execute(CollisionKind.Tetromino))
            {
                movementHandler.Reverse().TransitionY(ControlledShape);
                listener.Notify(ControlledShape.Bricks, Message.Landed);
            }
            if (collisionHandler.Execute(CollisionKind.UpperBound) && ControlledShape.Bricks.Any(brick => brick.state != BrickState.Falling))
            {
                listener.Notify(ControlledShape.Bricks, Message.HitUpperBound);
            }
        }   
    }

    public class TeleportAtShape : TransitionManager
    {
        private readonly Shape targetShape;

        public TeleportAtShape(Shape controlledShape, Shape targetShape) : base(controlledShape)
        {
            this.targetShape = targetShape;
        }

        public override void Execute()
        {

            var targetBounds = targetShape?.Bricks.Select(brick => brick.bounds).ToList(); 

            for (var i = 0; i < 4; i++)
            {
                if (targetBounds != null)
                    movementHandler.CloneBrickPosition(ControlledShape.Bricks.ElementAt(i), targetBounds.ElementAt(i));
            }
        }
    }

    // output class [in order to perform an in-game transition create a new instance of the below. For teleport use 2 args overload.

    public class TransitionHandler
    {
        private readonly Shape controlledShape;

        public TransitionHandler(Shape controlledShape)
        {
            this.controlledShape = controlledShape;
        }

        public void InvokeTransition(TransitionManager transitor)
        {
            transitor.Execute();
        }

        public void Execute(MovementKind movementKind)
        {
            TransitionManager transitor;

            switch (movementKind)
            {
                case MovementKind.Down:
                    transitor = new TransitionDown(controlledShape);
                    InvokeTransition(transitor);
                    break;
                case MovementKind.Up:
                    transitor = new TransitionUp(controlledShape);
                    InvokeTransition(transitor);
                    break;
                case MovementKind.Left:
                    transitor = new TransitionLeft(controlledShape);
                    InvokeTransition(transitor);
                    break;
                case MovementKind.Right:
                    transitor = new TransitionRight(controlledShape);
                    InvokeTransition(transitor);
                    break;
                case MovementKind.Rotate:
                    transitor = new Rotate(controlledShape);
                    InvokeTransition(transitor);
                    break;
                case MovementKind.Teleport:
                    throw new ArgumentException("For teleport use two args overload");
            }
        }

        public void Execute(MovementKind teleportOnly, Shape targetShape)
        {
            if (teleportOnly != MovementKind.Teleport)
                throw new ArgumentException(
                    "Two args overload can be used only on teleport method; for other kinds of movement use the one arg overload");
            var transitor = new TeleportAtShape(controlledShape, targetShape);
            InvokeTransition(transitor);
        }
    }

    #endregion

    //update logic

    #region GameFlow

    public class RandomBagger
    {
        private readonly Queue<ShapeKind> randomBag = new Queue<ShapeKind>();
        public Queue<ShapeKind> RandomBag => randomBag;

        public void GetQueue()
        {
            var temp = new List<ShapeKind>();

            var values = (int[])Enum.GetValues(typeof(ShapeKind));

            for (byte i = 0; i < 9; i++)
                temp.AddRange(values.Select(value => (ShapeKind)value));

            var r = new Random();
            var _temp = temp.OrderBy(x => r.Next());

            foreach (ShapeKind shape in _temp)
                randomBag.Enqueue(shape);
        }
    }

    public class FallingShape 
    {
        private Shape currentShape;
        public Shape CurrentShape => currentShape;
        private readonly ShapeSpawner shapeSpawner;
        private TransitionDown moveShapeDown;

        public FallingShape()
        {
            shapeSpawner = new ShapeSpawner();
        }

        public void CreateNew(ShapeKind randomShapeKind)
        {
           currentShape = shapeSpawner.SetKind(SmartShapeKind.Falling).AutoSpawn(randomShapeKind);
           currentShape.Bricks.SetState(BrickState.Falling);
           Shared.Tetrominos.Add(currentShape);
           moveShapeDown = new TransitionDown(currentShape);
        }

        public void Update() 
        {
            if (currentShape.Bricks.TrueForAll(brick => brick.state == BrickState.Falling)) moveShapeDown.Execute();
        }
    }

    public class Ghost
    {
        private ShapeSpawner shapeSpawner;
        private TransitionHandler moveGhostShape;
        private MovementHandler move;
        private CollisionHandler collisionHandler;
        private Shape ghostShape;
        public Shape GhostShape => ghostShape;
        private readonly Func<Shape> fallingShape = () => Shared.Tetrominos.Find(
                    tetromino => tetromino.Bricks.TrueForAll(brick => brick.state == BrickState.Falling));

        public void CreateGhostShape()
        {
            shapeSpawner = new ShapeSpawner();
            ghostShape = shapeSpawner.SetKind(SmartShapeKind.Ghost).AutoSpawn(fallingShape.Invoke().ShapeKind);
            ghostShape.Bricks.SetState(BrickState.Ghost);
            moveGhostShape = new TransitionHandler(ghostShape);
            move = new MovementHandler(new BoardVariableHolder().BrickSize);
            collisionHandler = new CollisionHandler(ghostShape);
            Shared.Tetrominos.Add(ghostShape);
        }

        public void Update()
        {
           moveGhostShape.Execute(MovementKind.Teleport, fallingShape.Invoke());

            while (!collisionHandler.Execute(CollisionKind.Bottom) || !collisionHandler.Execute(CollisionKind.Tetromino))
            {
                move.TransitionY(ghostShape);

                if (collisionHandler.Execute(CollisionKind.Bottom) || collisionHandler.Execute(CollisionKind.Tetromino))
                {
                    move.Reverse().TransitionY(ghostShape);
                    break;
                }
            }

            if (!Shared.Tetrominos.Any(tetromino => tetromino.Bricks.Any(brick => brick.state == BrickState.Falling)))
                Shared.Tetrominos.Remove(ghostShape);
        }

    }

    public class UpdateLines
    {
        private readonly Subject listener;

        public UpdateLines()
        {
            listener = new Subject();
            listener.AddObserver(new Audio());
            listener.AddObserver(new Graphics());
            listener.AddObserver(new Logic());
        }

        public IEnumerable<int> MarkDead()
        {
            Shared.Tetrominos.SelectBricks()
                .GetRowsWhere(bricks => bricks.Count() == 10)
                .ToList()
                .ForEach(row => row.SetState(BrickState.Dead));

            var deadLines =
                Shared.Tetrominos.SelectBricks()
                    .GetRowsWhere(bricks => bricks.All(brick => brick.state == BrickState.Dead))
                    .ToList()
                    .SelectMany(row => row.Select(brick => brick.bounds.Center.Y))
                    .Distinct();

            if (deadLines.Count() != 0)
            {
                listener.Notify(
                    Shared.Tetrominos.SelectBricks().ToList().FindAll(brick => brick.state == BrickState.Dead),
                    Message.MarkedDead);
            }

            return deadLines;
        }

        public async void FillLines(IEnumerable<int> deadLines)
        {
            if (deadLines.Count() != 0)
            await Task.Delay(500);

        foreach (var tetromino in Shared.Tetrominos)
            {
                tetromino.Bricks.RemoveAll(brick => brick.state == BrickState.Dead);
            }

            Shared.Tetrominos.RemoveAll(shape => shape.Bricks.Count == 0);


            foreach (var line in deadLines.OrderByDescending(x => x).Reverse())
            {
                Shared.Tetrominos.SelectBricks()
                    .ToList()
                    .FindAll(brick => brick.bounds.Center.Y < line)
                    .ForEach(brick => new MovementHandler(new BoardVariableHolder().BrickSize).TransitionY(brick));
            }

        }
    }

    public class Preview
    {
        private Shape preview;
        public Shape NextShape => preview;

        public void CreateNew(ShapeKind nextShape)
        {
            preview = new ShapeSpawner().SetKind(SmartShapeKind.Preview).AutoSpawn(nextShape);
        }
    }

    public class KeyboardEventManager : GameComponent
    {
        private readonly List<Keys> pressedKeys = new List<Keys>(), toSendKeys = new List<Keys>();
        public List<Keys> PressedKeys => pressedKeys;
        private double intervalP, intervalR;       


        public KeyboardEventManager(Game game)
            :base(game)
        { }

        public override void Update(GameTime gameTime)
        {
            intervalR += gameTime.ElapsedGameTime.TotalMilliseconds;
            intervalP += gameTime.ElapsedGameTime.TotalMilliseconds;

            base.Update(gameTime);

            foreach (Keys key in Enum.GetValues(typeof(Keys)))
            {
                if (Keyboard.GetState().IsKeyDown(key) && intervalP > 200)
                {
                    pressedKeys.Add(key);
                    KeyPressed?.Invoke(key);
                    intervalP = 0;
                }
                if (Keyboard.GetState().IsKeyUp(key) && intervalR < 200)
                {
                    if (pressedKeys.Contains(key))
                    {
                        pressedKeys.Remove(key);
                    
                        KeysRealeased?.Invoke(key);
                        intervalR = 0;

                    }
                }
            }
        }

        public event Action<Keys> KeyPressed;
        public event Action<Keys> KeysRealeased;
    }


    public class KeyboardEventSolver
    {
        public List<BitFontButton> buttonList { set; get; } 
        private TransitionHandler mover;
        private readonly Func<Shape> fallingShape =
            () => Shared.Tetrominos.Find(tetromino => tetromino.Bricks.All(brick => brick.state == BrickState.Falling));

        private readonly Func<Shape> ghostShape =
            () => Shared.Tetrominos.Find(tetromino => tetromino.Bricks.All(brick => brick.state == BrickState.Ghost));

        public void InsideTheGame(Keys key)
        {
            if (
                !Shared.Tetrominos.Any(
                    tetromino => tetromino.Bricks.TrueForAll(brick => brick.state == BrickState.Falling)))
                return;

            mover = new TransitionHandler(fallingShape.Invoke());
            switch (key)
            {
                    case Keys.Space:
                        mover.Execute(MovementKind.Rotate);
                    break;
                    case Keys.Left:
                        mover.Execute(MovementKind.Left);
                    break;
                    case Keys.Right:
                        mover.Execute(MovementKind.Right);
                    break;
                    case Keys.Down:
                        mover.Execute(MovementKind.Teleport, ghostShape.Invoke());
                    break;
            }
        }

        public void MainMenu(Keys key)
        {
            if (buttonList == null) return;
            switch (key)
            {
                case Keys.Up:
                    new ButtonEventHandler(buttonList).ArrowUp();
                    break;
                case Keys.Down:
                    new ButtonEventHandler(buttonList).ArrowDown();
                    break;
                case Keys.Enter:
                    new ButtonEventHandler(buttonList).Enter();
                    break;
            }
        }

        public void GameOverMenu(Keys key)
        {
            if (key == Keys.Enter)
            {

                new GameOverEventResolver().Enter();
            }
            else
            {
                new GameOverEventResolver().WriteName(key);
            }
        }
    }

    public abstract class Observer
    {
        public abstract void OnNotify<T>(T callingObject, Message message)
            where T : IEnumerable<Brick>;
    }

    public class Logic : Observer
    {   

        public override void OnNotify<T>(T callingObject, Message message)
        {
            switch (message)
            {
                case Message.Landed:
                    callingObject.SetState(BrickState.Laying);
                    var updateLines = new UpdateLines();
                    var deadLines = updateLines.MarkDead();
                    updateLines.FillLines(deadLines);
                    break;
                case Message.MarkedDead:
                    new ScoreCounter().CalculateScore(callingObject);
                    break;    
                case Message.HitUpperBound:
                    Shared.GameState = GameState.GameOver;
                    break;
            }
        }

    }

    public class Audio : Observer
    {
        public override void OnNotify<T>(T callingObject, Message message)
        {
            switch (message)
            {
                case Message.Landed:
                    //playSound
                    break;
                case Message.MarkedDead:
                    //playSound
                    break;
            }
        }
    }

    public class Graphics : Observer
    {
        public override void OnNotify<T>(T callingObject, Message message)
        {
            switch (message)
            {
                case Message.Landed:
                    callingObject.ToList().ForEach(brick => brick.UpdateTextures());
                    break;
                case Message.MarkedDead:
                    callingObject.ToList().ForEach(brick => brick.SetTexture(Game1.DeadTexture));
                    break;
            }
        }

    }

    public class Subject
    {
        private readonly List<Observer> observers = new List<Observer>();
        public readonly Action<Observer> AddObserver, RemoveObserver;

        public Subject()
        {
            AddObserver = observer => observers.Add(observer);
            RemoveObserver = observer => observers.Remove(observer);
        }

        public void Notify<T> (T sender, Message message)
            where T : IEnumerable<Brick>
        {
            if (sender.All(brick => brick.state != BrickState.Ghost))
                 observers.ForEach(observer => observer.OnNotify(sender, message));
        }


    }

    public struct BoardVariableHolder
    {
        public Rectangle StartingSpawningPoint => new Rectangle(BoardCentre.X, 20, 20, 20);
        public Rectangle GhostSpawningPoint => new Rectangle(StartingSpawningPoint.X, 0, 20, 20);
        public Rectangle PreviewShapeSpawningPoint => new Rectangle(550, 100, 20, 20);
        public int MaxX => 480;
        public int MinX => 300;
        public int MaxY => 460;
        public int MinY => 40;
        public static Point BoardCentre { get; set; }
        public int BrickSize => 20;
    }

    #endregion

    //Collision Logic

    #region Collisions


    //gamelogic collision resolvers

    public abstract class CollisionManager 
    {
        protected BoardVariableHolder VariablePasser = new BoardVariableHolder();

        public abstract bool Execute(Shape shape);
    }

    public class CollisionWithTetromino : CollisionManager
    {
        public override bool Execute(Shape shape)
        {
            var tetrominosBounds =
                Shared.Tetrominos.SelectMany(
                    tetromino =>
                        tetromino.Bricks.FindAll(brick => brick.state != BrickState.Falling && brick.state != BrickState.Ghost)
                            .Select(brick => brick.bounds)).ToList();


            var testedShapeBounds = shape.Bricks.Select(brick => brick.bounds).ToList();

            return
                testedShapeBounds.Any(
                    testedRectangle =>
                        tetrominosBounds.Any(tetroRectangle => tetroRectangle.Intersects(testedRectangle)));
        }
    }

    public class CollisionRight : CollisionManager
    {
        public override bool Execute(Shape shape)
        {
            var testedShapeBounds = shape.Bricks.Select(brick => brick.bounds).ToList();

            return testedShapeBounds.Any(bound => bound.X > VariablePasser.MaxX);
        }
    }

    public class CollisionLeft : CollisionManager
    {
        public override bool Execute(Shape shape)
        {
            var testedShapeBounds = shape.Bricks.Select(brick => brick.bounds).ToList();

            return testedShapeBounds.Any(bound => bound.X < VariablePasser.MinX);
        }
    }

    public class CollisionBottom : CollisionManager
    {
        public override bool Execute(Shape shape)
        {
            var testedShapeBounds = shape.Bricks.Select(brick => brick.bounds).ToList();

            return testedShapeBounds.Any(bound => bound.Y > VariablePasser.MaxY);
        }
    }

    public class CollisionUpperBound : CollisionManager
    {
        public override bool Execute(Shape shape)
        {
            var testedShapeBounds = shape.Bricks.Select(brick => brick.bounds).ToList();

            return testedShapeBounds.Any(bound => bound.Y <= VariablePasser.MinY);
        }
    }

    //outptut class [it takes Shape and passes it to the resolvers // in order to resolve a collision create a new instance of the below]

    public class CollisionHandler
    {
        private readonly Shape testedShape;
        private CollisionManager collisionManager;

        public CollisionHandler(Shape testedShape)
        {
            this.testedShape = testedShape;
        }

        private bool InvokeCollision()
        {
            return collisionManager.Execute(testedShape);
        }

        public bool Execute(CollisionKind collisionKind)
        {
            switch (collisionKind)
            {
                case CollisionKind.Tetromino:
                    collisionManager = new CollisionWithTetromino();
                    return InvokeCollision();
                case CollisionKind.RightBound:
                    collisionManager = new CollisionRight();
                    return InvokeCollision();
                case CollisionKind.LeftBound:
                    collisionManager = new CollisionLeft();
                    return InvokeCollision();
                case CollisionKind.Bottom:
                    collisionManager = new CollisionBottom();
                    return InvokeCollision();
                case CollisionKind.UpperBound:
                    collisionManager = new CollisionUpperBound();
                    return InvokeCollision();
            }
            return false;
        }
    }
}

#endregion



