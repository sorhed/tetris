﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGame.Extended.BitmapFonts;

namespace TETRIS
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        public static Texture2D RegularBrickTexture, GhostTexture, PreviewTexture, DeadTexture;
        public static SpriteFont Arcarde;
        private TheGame gameplay;
        private MainMenu mainMenu;
        private KeyboardEventManager keyboardEventManager;
        private KeyboardEventSolver keyboardEventSolver;
        private List<IScreen> screens;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            var Centre = new Point(GraphicsDevice.Viewport.Width / 2,
                GraphicsDevice.Viewport.Height / 2);
            BoardVariableHolder.BoardCentre = new Point(Centre.X, Centre.Y);
            gameplay = new TheGame(this, this.Content);
            mainMenu = new MainMenu(this, this.Content);
            keyboardEventManager = new KeyboardEventManager(this);
            keyboardEventSolver = new KeyboardEventSolver();

            Components.Add(gameplay);
            Components.Add(mainMenu);
            Components.Add(keyboardEventManager);
            screens = new List<IScreen> {gameplay, mainMenu};
            Shared.GameState = GameState.MainMenu;
            keyboardEventSolver.buttonList = mainMenu.buttons;
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            RegularBrickTexture = this.Content.Load<Texture2D>("element_blue_square");
            GhostTexture = this.Content.Load<Texture2D>("element_grey_square");
            PreviewTexture = this.Content.Load<Texture2D>("element_green_square");
            DeadTexture = this.Content.Load<Texture2D>("element_red_square");
            Arcarde = this.Content.Load<SpriteFont>("Arcade");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            // TODO: Add your update logic here

            switch (Shared.GameState)
            {
                    case GameState.TheGame:
                         if (!screens.Contains(gameplay))
                               screens.Add(gameplay);
                         gameplay.UpdateActive = true;
                         gameplay.DrawActive = true;
                         screens.FindAll(screen => screen != gameplay).ForEach(screen => screen.DrawActive = false);
                         screens.FindAll(screen => screen != gameplay).ForEach(screen => screen.UpdateActive = false);
                         keyboardEventManager.KeyPressed -= keyboardEventSolver.InsideTheGame;
                         keyboardEventManager.KeyPressed += keyboardEventSolver.InsideTheGame;
                    break;
                    case GameState.MainMenu:
                          if (!screens.Contains(mainMenu))
                             screens.Add(mainMenu);
                         mainMenu.UpdateActive = true;
                         mainMenu.DrawActive = true;
                         screens.FindAll(screen => screen != mainMenu).ForEach(screen => screen.DrawActive = false);
                         screens.FindAll(screen => screen != mainMenu).ForEach(screen => screen.UpdateActive = false);
                         keyboardEventManager.KeyPressed -= keyboardEventSolver.MainMenu;
                         keyboardEventManager.KeyPressed += keyboardEventSolver.MainMenu;
                    break;
                    case GameState.PauseMenu:
                    gameplay.UpdateActive = false;
                    IsMouseVisible = true;
                    break;
                    case GameState.GameOver:
                        keyboardEventManager.KeysRealeased -= keyboardEventSolver.GameOverMenu;
                        keyboardEventManager.KeyPressed -= keyboardEventSolver.GameOverMenu;

                        keyboardEventManager.KeysRealeased += keyboardEventSolver.GameOverMenu;
                        keyboardEventManager.KeyPressed += keyboardEventSolver.GameOverMenu;
                    break;
            }

            screens.FindAll(screen => screen.UpdateActive)
                .ForEach(screen => screen.Update(gameTime));
                

            base.Update(gameTime);
        }

        private void KeyboardEventManager_KeyRealeased(Keys obj)
        {
            throw new NotImplementedException();
        }



        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            screens.FindAll(screen => screen.DrawActive).ForEach(screen => screen.Draw(gameTime));
            // TODO: Add your drawing code here
        }
    }
}
