﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Threading.Tasks;
using MonoGame.Extended.BitmapFonts;

namespace TETRIS
{
    public class ScoreCounter
    {
        private readonly int[] multipliers = {1, 3, 6, 9, 12, 15};

        public void CalculateScore<T>(T bricks)
            where T : IEnumerable<Brick>
        {
            Shared.ScoreCounter += Shared.SpeedMultiplier*multipliers[(bricks.Count()/10) - 1]*10;
        }
    }

    public class HighScore
    {
        public int Score { get; set; }
        public string Name { get; set; }
    }

    public class HighScoreManager 
    {
        private string saveStorage = "C:/Users/MrMM/Documents/Visual Studio 2015/Projects/TETRIS/TETRIS/Scores/Hscores.txt";
        public List<HighScore> AllHighScores { get; set; }


        public HighScoreManager()
        {
            AllHighScores = GetHighScores().ToList();
        }

        public IEnumerable<HighScore> GetHighScores()
        {
            string[] highScoreText = File.ReadAllLines(saveStorage);

            HighScore[] highScores = new HighScore[highScoreText.Length];

            for (int i = 0; i < highScoreText.Length; i++)
            {
                string[] tokens = highScoreText[i].Split(',');

                string name = tokens[0];
                int score = Convert.ToInt32(tokens[1]);

                yield return highScores[i] = new HighScore() {Name = name, Score = score};
            }
        }

        public void SaveHighScores<T>(T highScores)
            where T : IEnumerable<HighScore>
        {
            string stringStream = "";

            highScores.ToList().ForEach(line => stringStream += $"{line.Name}, {line.Score}\n");
            File.WriteAllText(saveStorage, stringStream);
        }
    }

    public class GameOverEventResolver
    {
        private HighScore currentHighScore = new HighScore();
        private IEnumerable<HighScore> currentHighScores = new List<HighScore>();

        public HighScore CurrentHighScore => currentHighScore;


        public void WriteName (Keys keysReleased)
        { 
             Shared.PlayerName += keysReleased.ToString();
        }

        public void Enter()
        {
            var hsManager = new HighScoreManager();

            currentHighScore.Score = Shared.ScoreCounter;
            currentHighScore.Name = Shared.PlayerName;

            currentHighScores = hsManager.AllHighScores;
            hsManager.AllHighScores.Add(currentHighScore);

            hsManager.SaveHighScores(currentHighScores.OrderByDescending(highscore => highscore.Score));

            Shared.GameState = GameState.ScoreTable;
        }


    }


}
