﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using MonoGame.Extended.BitmapFonts;

namespace TETRIS
{
   public static class ExtensionMethods
    {
       public static IEnumerable<Brick> BricksWhere(this IEnumerable<Shape> shapes, Func<Brick, bool> predicate)
        {
            return from shape in shapes from brick in shape.Bricks where predicate(brick) select brick;
        }

       public static void SetState(this IEnumerable<Brick> bricks, BrickState state)
       {
           foreach (var brick in bricks)
           {
               brick.state = state;
           } 
       }

       public static IEnumerable<Brick> SelectBricks(this IEnumerable<Shape> shapes)
       {
           return shapes.SelectMany(shape => shape.Bricks).ToList();
       }

       public static IEnumerable<IEnumerable<Brick>> GetRowsWhere(this IEnumerable<Brick> bricks, Func<IEnumerable<Brick>, bool> predicate)
       {
           var tempListNumbers = bricks.Select(brick => brick.bounds.Center.Y).Distinct().ToList();
           var rows = new List<Brick>[tempListNumbers.Count];

           for (int i = 0; i < rows.Length; i++)
           {
               rows[i] = bricks.ToList().FindAll(brick => brick.bounds.Center.Y == tempListNumbers.ElementAt(i) && brick.state != BrickState.Ghost);
           }

           foreach (var row in rows)
           {
               if (predicate(row))
                   yield return row;
           }

       }

        public static Task Delay(double milliseconds)
        {
            var tcs = new TaskCompletionSource<bool>();
            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Elapsed += (o, e) => tcs.TrySetResult(true);
            timer.Interval = milliseconds;
            timer.AutoReset = false;
            timer.Start();
            return tcs.Task;
        }

    }

}
