﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.BitmapFonts;

namespace TETRIS
{
    public class TheGame : DrawableGameComponent, IScreen
    {
        private BitmapFont font;
        private Texture2D gameOverTexture, inputFieldTexture;
        public GameOverMenu GameOverMenu;

        private readonly SpriteBatch spriteBatch;
        private readonly ContentManager content;
        private readonly RandomBagger bagOfShapes = new RandomBagger();
        private readonly FallingShape activeShape = new FallingShape();
        private readonly Preview preview = new Preview();
        private readonly Ghost ghostShape = new Ghost();
        private readonly Painter painter;

        private float fallTimer = 0.5f,
            fallTimer0 = 0.5f,
            startTimer = 3f;

        private bool isPaused;

        public bool UpdateActive { get; set; }
        public bool DrawActive { get; set; }

        public TheGame(Game game, ContentManager content)
            : base(game)
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            this.content = content;
            this.content.RootDirectory = "Content";
            Shared.SpeedMultiplier = 1;
            painter = new Painter(spriteBatch);
        }

        protected override void LoadContent()
        {
            font = content.Load<BitmapFont>("futureSquare");
            gameOverTexture = content.Load<Texture2D>("GameOverTexture");
            inputFieldTexture = content.Load<Texture2D>("input_field");
        }

        public override void Update(GameTime gameTime)
        {
            isPaused = Shared.GameState == GameState.PauseMenu || Shared.GameState == GameState.GameOver;
            var elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;
            fallTimer0 -= elapsed;
            startTimer -= elapsed;

            if (Shared.GameState == GameState.GameOver)
            {
                GameOverMenu = new GameOverMenu(gameOverTexture, new Point(GraphicsDevice.Viewport.Width / 2, GraphicsDevice.Viewport.Height / 2), inputFieldTexture);
                GameOverMenu.Update();
            }

            if (Shared.Tetrominos.SelectBricks().Any(brick => brick.state == BrickState.Dead || isPaused || startTimer > 0)) return;

            // TODO: Add your update logic here
            if (!Shared.Tetrominos.Any(tetromino => tetromino.Bricks.Any(brick => brick.state == BrickState.Falling)))
            {
                if (bagOfShapes.RandomBag.Count() < 2) bagOfShapes.GetQueue();
                activeShape.CreateNew(bagOfShapes.RandomBag.Dequeue());
                preview.CreateNew(bagOfShapes.RandomBag.Peek());
                ghostShape.CreateGhostShape();
            }


            if (fallTimer0 < 0)
            {
                activeShape.Update();
                fallTimer0 = fallTimer;
            }
            ghostShape.Update();

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            if (startTimer > 0) return;
            spriteBatch.Begin();
            painter.Draw(Shared.Tetrominos.SelectBricks());
            painter.Draw(preview.NextShape.Bricks);
            spriteBatch.DrawString(font, Shared.ScoreCounter.ToString(), Vector2.One, Color.DarkRed);
            painter.Draw(GameOverMenu?.PaintMe);
            if (Shared.GameState == GameState.GameOver && GameOverMenu != null)
            {
                painter.DrawSpriteFont(Game1.Arcarde, Shared.PlayerName,new Vector2(GameOverMenu.InputField.Bounds.X + 10, GameOverMenu.InputField.Bounds.Y + 10), 1f);
            }
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
