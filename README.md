Tetris MonoGame project

My first "serious" gameDev project; my goal here was to familiarize myself with basic principles of designing game code, the essential design patterns (game and draw loops) as well as OO in general. 

Unfortunately, due to lack of proper planning the game now is in serious spaghetti state, altough the gameplay itself is working as expected. GUI, menus and HScores however are still waiting to be finished.