﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TETRIS
{
    public static class Shared
    {
        public static List<Shape> Tetrominos = new List<Shape>();
        public static GameState GameState { get; set; }
        public static int ScoreCounter { get; set; }
        public static int SpeedMultiplier { get; set; }
        public static string PlayerName { get; set; } = "";
    }
}
