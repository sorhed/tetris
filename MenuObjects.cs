﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.BitmapFonts;


namespace TETRIS
{
    #region enums

    public enum BState
    {
        Hover,
        Up,
        JustReleased,
        Down
    }

    public enum ButtonKind
    {
        Start,
        Preferences,
        Scores,
        Exit
    }

#endregion
    public class Button : IDrawable
    {
        private Rectangle buttonRectangle;
        private BState bState;
        private readonly Texture2D activeTexture, downTexture, upTexture;
        private double timer;
        public double Timer
        {
            get { return timer; }
            set { timer = value; }
        }

        public BState ButtonState
        {
            get { return bState; }
            set { bState = value; }
        }

        public Rectangle ButtonRectangle => buttonRectangle;
        public Texture2D Texture { get; private set; }
        public Vector2 Bounds => new Vector2(buttonRectangle.X, buttonRectangle.Y);


        public Button(Rectangle buttonRectangle, Texture2D activeTexture, Texture2D downTexture, Texture2D upTexture)
        {
            this.buttonRectangle = buttonRectangle;
            this.activeTexture = activeTexture;
            this.downTexture = downTexture;
            this.upTexture = upTexture;
            bState = BState.Up;
            timer = 0.0;
        }
        public void UpdateTextures()
        {
            switch (bState)
            {
                case BState.Up:
                    Texture = upTexture;
                    break;
                case BState.Down:
                    Texture = downTexture;
                    break;
                case BState.Hover:
                    Texture = downTexture;
                    break;
                case BState.JustReleased:
                    Texture = activeTexture;
                    break;
            }
        }
    }


    public class Painter
    {
        private SpriteBatch spriteBatch;

        public Painter(SpriteBatch spriteBatch)
        {
            this.spriteBatch = spriteBatch;
        }

        public void Draw (IDrawable drawableObject)
        {
            spriteBatch.Draw(drawableObject.Texture, drawableObject.Bounds);
        }

        public void Draw(IEnumerable<IDrawable> drawableObjects)
        {
            drawableObjects?.ToList().ForEach(dobject => Draw(dobject));
        }

        public void DrawStringButton(BitFontButton button)
        {
            var colourToDraw = button.IsActive ? button.ActiveColour : button.NonActiveColour;

            spriteBatch.DrawString(button.ThisFont, button.ButtonFlavor, new Vector2(button.ButtonBounds.X, button.ButtonBounds.Y), colourToDraw);
        }

        public void DrawSpriteFont(SpriteFont spriteFont, string flavour, Vector2 position, float scale)
        {
            var fontOr = spriteFont.MeasureString(flavour) / 2;
            var builder = new StringBuilder(flavour);

            spriteBatch.DrawString(spriteFont, builder, position, Color.Black, 0, fontOr, scale, SpriteEffects.None, 0);

        }
    }

    public class BitFontButton
    {
        public BitmapFont ThisFont { get; }
        public ButtonKind ButtonKind { get; }
        public Rectangle ButtonBounds { get; }
        public string ButtonFlavor { get; }
        public bool IsActive { get; set; }
        public Color NonActiveColour { get; }
        public Color ActiveColour { get; }

        public BitFontButton(string buttonFlavor, ButtonKind buttonKind, BitmapFont thisFont, Vector2 position, Color nonActiveColour, Color activeColour)
        {

            ButtonBounds = thisFont.GetStringRectangle(buttonFlavor, position);
            ThisFont = thisFont;
            ButtonFlavor = buttonFlavor;
            ButtonKind = buttonKind;
            ActiveColour = activeColour;
            NonActiveColour = nonActiveColour;
        }


    }

    public class ButtonEventHandler
    {
        private readonly List<BitFontButton> buttons;
        public static ButtonEventHandler Instance;

        public ButtonEventHandler(List<BitFontButton> buttons)
        {
            this.buttons = buttons;
        }

        public void ArrowDown()
        {
            var activeButton = buttons.Find(button => button.IsActive);
            var activeButtonIndex = buttons.IndexOf(activeButton);

            if (activeButtonIndex != buttons.Count - 1)
            {
                activeButton.IsActive = false;
                buttons.ElementAt(activeButtonIndex + 1).IsActive = true;
            }
        }

        public void ArrowUp()
        {
            var activeButton = buttons.Find(button => button.IsActive);
            var activeButtonIndex = buttons.IndexOf(activeButton);

            if (activeButtonIndex != 0)
            {
                activeButton.IsActive = false;
                buttons.ElementAt(activeButtonIndex - 1).IsActive = true;
            }
        }

        public void Enter()
        {
            switch (buttons.Find(button => button.IsActive).ButtonKind)
            {
                case ButtonKind.Start:
                    Shared.GameState = GameState.TheGame;
                    break;
                case ButtonKind.Preferences:
                    Shared.GameState = GameState.PauseMenu;
                    break;
                case ButtonKind.Scores:
                    Shared.GameState = GameState.ScoreTable;
                    break;
                case ButtonKind.Exit:
                    //exit game
                    break;
            }
        }

    }

    public class GameOverMenu : IDrawable
    {
        private readonly GUIInputField inputField;
        public GUIInputField InputField => inputField;

        public Texture2D Texture { get; }
        public Vector2 Bounds { get; }
        public List<IDrawable> PaintMe;

        public GameOverMenu(Texture2D screenTexture, Point centre, Texture2D inputFieldTexture)
        {
            Texture = screenTexture;
            Bounds = new Vector2(centre.X - screenTexture.Width/2, centre.Y);

            inputField = new GUIInputField(inputFieldTexture,
                new Vector2(centre.X - inputFieldTexture.Width / 2, centre.Y + inputFieldTexture.Height));

            PaintMe = new List<IDrawable> {this};
        }

        public void Update()
        {
            if (!PaintMe.Contains(inputField))
                PaintMe.Add(inputField);      

        }
    }

    public class GUIInputField : IDrawable
    {
        public Texture2D Texture { get; }
        public Vector2 Bounds { get; }

        public GUIInputField(Texture2D texture, Vector2 bounds)
        {
            Texture = texture;
            Bounds = bounds;
        }


    }
}
