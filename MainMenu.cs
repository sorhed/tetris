﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.BitmapFonts;

namespace TETRIS
{
    class MainMenu : DrawableGameComponent, IScreen
    {
        private readonly SpriteBatch spriteBatch;
        private readonly GraphicsDevice graphics;
        private readonly ContentManager content;
        private readonly Painter painter;
        private BitmapFont font;
        private readonly Color nonActiveColour = Color.ForestGreen;
        private readonly Color activeColour = Color.DarkBlue;

        public readonly List<BitFontButton> buttons = new List<BitFontButton>();
        private readonly string[] buttonStrings = {"Start", "Preferences", "Highscore", "Quit"};

        private int buttonHeight = 62,
            buttonWidth = 392;

        private readonly Vector2[] buttonPositions = new Vector2[4];

        public bool UpdateActive { get; set; }
        public bool DrawActive { get; set; }

        public MainMenu(Game game, ContentManager content)
            : base(game)
        {
            graphics = game.GraphicsDevice;
            spriteBatch = new SpriteBatch(GraphicsDevice);
            this.content = content;
            this.content.RootDirectory = "Content";
            painter = new Painter(spriteBatch);
        }

        protected override void LoadContent()
        {
            font = content.Load<BitmapFont>("futureSquare");
        }

        public override void Update(GameTime gameTime)
        {
            if (buttons.Count == 0)
            {

                int x = graphics.Viewport.Width/2 - buttonWidth/4;
                int y = graphics.Viewport.Height/2 - buttonHeight/2*buttonPositions.Length -
                        buttonHeight%2*buttonPositions.Length/2;

                for (int i = 0; i < buttonPositions.Length; i++)
                {
                    buttonPositions[i] = new Vector2(x, y);
                    y += buttonHeight;
                }

                for (int i = 0; i < 4; i++)
                {
                    buttons.Add(new BitFontButton(buttonStrings[i], (ButtonKind) i, font, buttonPositions[i], nonActiveColour, activeColour));
                }

                buttons.ElementAt(0).IsActive = true;
            }

            base.Initialize();
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
           buttons.ForEach(button => painter.DrawStringButton(button));
            spriteBatch.End();
        }


    }
}
